#!/usr/bin/env python
# coding: utf-8

# # ML_Gesture_Recognition using Sklearn
# #By- Aarush Kumar
# #Dated: October 03,2021

# In[1]:


from IPython.display import Image
Image(url='https://cdn.analyticsvidhya.com/wp-content/uploads/2018/10/MULTITOUCH_AND_GESTURE_RECOGNITION.jpg')


# In[2]:


import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split,GridSearchCV
from sklearn.metrics import confusion_matrix,classification_report,accuracy_score,plot_confusion_matrix
from sklearn import tree
import matplotlib.pyplot as plt
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras import backend as K
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier
import matplotlib.pyplot as plt
import copy
from sklearn.model_selection import KFold
import time
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import f1_score


# In[3]:


rock_data = pd.read_csv("/home/aarush100616/Downloads/Projects/ML gesture recognition/Data/0.csv", header=None )
scissors_data = pd.read_csv("/home/aarush100616/Downloads/Projects/ML gesture recognition/Data/1.csv", header=None )
paper_data = pd.read_csv("/home/aarush100616/Downloads/Projects/ML gesture recognition/Data/2.csv", header=None )
ok_data = pd.read_csv("/home/aarush100616/Downloads/Projects/ML gesture recognition/Data/3.csv", header=None )
data = pd.concat([rock_data, scissors_data, paper_data, ok_data], axis = 0)
X = data.drop(data.columns[-1],axis=1)
y = data[data.columns[-1]]
#Split data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
X_train.head(5)


# In[4]:


#Check the number of times each gesture occurs in training data.
y_train.value_counts()
print("Dataset samples:",len(y))


# In[5]:


print(__doc__)

import numpy as np
import matplotlib.pyplot as plt
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.datasets import load_digits
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit

def plot_learning_curve(estimator, title, X, y, axes=None, ylim=None, cv=None,
                        n_jobs=None, train_sizes=np.linspace(.1, 1.0, 5)):
    if axes is None:
        _, axes = plt.subplots(1, 1, figsize=(20, 5))

    axes[0].set_title(title)
    if ylim is not None:
        axes[0].set_ylim(*ylim)
    axes[0].set_xlabel("Training examples")
    axes[0].set_ylabel("Predictive Accuracy")

    train_sizes, train_scores, test_scores, fit_times, _ =         learning_curve(estimator, X, y, cv=cv, n_jobs=n_jobs,
                       train_sizes=train_sizes,
                       return_times=True,verbose=2)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    fit_times_mean = np.mean(fit_times, axis=1)
    fit_times_std = np.std(fit_times, axis=1)

    # Plot learning curve
    axes[0].grid()
    axes[0].fill_between(train_sizes, train_scores_mean - train_scores_std,
                         train_scores_mean + train_scores_std, alpha=0.1,
                         color="r")
    axes[0].fill_between(train_sizes, test_scores_mean - test_scores_std,
                         test_scores_mean + test_scores_std, alpha=0.1,
                         color="g")
    axes[0].plot(train_sizes, train_scores_mean, 'o-', color="r",
                 label="Training score")
    axes[0].plot(train_sizes, test_scores_mean, 'o-', color="g",
                 label="Cross-validation score")
    axes[0].legend(loc="best")
    print("CV Scores:",test_scores_mean)

    return plt


# In[6]:


def r2(a,b):
    this_correlation = np.corrcoef(a, b)[0,1]
    this_r2 = this_correlation**2
    return this_r2


# In[7]:


def plot_confusion(title, model, X_train, y_train, X_test, y_test):
    svm_confusion_matrix = plot_confusion_matrix(model, X_train, y_train,
                      display_labels=['Rock','Scissors', 'Paper', 'Ok'],
                      cmap=plt.cm.YlOrBr)
    svm_confusion_matrix.ax_.set_title(title + " Confusion Matrix (Training Set)")
    plt.show()

    svm_confusion_matrix = plot_confusion_matrix(model, X_test, y_test,
                          display_labels=['Rock','Scissors', 'Paper', 'Ok'],
                          cmap=plt.cm.YlOrBr)
    svm_confusion_matrix.ax_.set_title(title + " Confusion Matrix (Testing Set)")
    plt.show()


# ## Decision Tree

# In[8]:


#Estimate parameters
decision_tree = tree.DecisionTreeClassifier(max_depth=10, 
                                  criterion='entropy',
                                  min_samples_leaf=10,
                                  min_samples_split=5,
                                  random_state=0)
decision_tree.fit(X_train, y_train)

y_train_pred = decision_tree.predict(X_train)
y_test_pred = decision_tree.predict(X_test)

print('Decision Tree Train Accuracy' , accuracy_score(y_train, y_train_pred))
print('Decision Tree Test Accuracy' , accuracy_score(y_test, y_test_pred))

cv = KFold(n_splits=10, random_state=0, shuffle=True)

fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(decision_tree, "Initial Decision Tree Learning Rate", 
                    X_train, y_train, axes=[axes], ylim=(0.4, 1.01),
                    cv=cv, n_jobs=4)


# In[9]:


dt_params = {
    "criterion":['gini','entropy'],
    "max_depth":range(5,30),
    "min_samples_leaf":range(1,5),
    "min_samples_split":range(1,5)
}
decision_tree = tree.DecisionTreeClassifier()


grid = GridSearchCV(decision_tree,
                    param_grid = dt_params,
                    cv=10,
                    verbose=1,
                    n_jobs=-1
)
grid.fit(X_train,y_train)
print(grid.best_params_)


# In[10]:


start_time = time.time();
decision_tree = tree.DecisionTreeClassifier(max_depth=20,
                                  criterion='gini',
                                  random_state=0,
                                  min_samples_leaf=1,
                                  min_samples_split=3)
decision_tree.fit(X_train, y_train)
print("Training time:",time.time()-start_time)

start_time = time.time();
y_train_pred = decision_tree.predict(X_train)
y_test_pred = decision_tree.predict(X_test)
print("Prediction Time:",time.time()-start_time, "for",len(X_train) + len(X_test),"samples")

print('Decision Tree Train Accuracy' , accuracy_score(y_train, y_train_pred))
print("Decision Tree Train r2 score:",r2(y_train, y_train_pred))
print('Decision Tree Test Accuracy' , accuracy_score(y_test, y_test_pred))
print("Decision Tree Test r2 score:",r2(y_test, y_test_pred))
print("F1 SCORE:",round(f1_score(y_test, y_test_pred, average='micro'),3))


# In[11]:


cv = KFold(n_splits=10, random_state=0, shuffle=True)

estimator = tree.DecisionTreeClassifier(max_depth=20,
                                  criterion='gini',
                                  random_state=0,
                                  min_samples_leaf=1,
                                  min_samples_split=3)
fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(estimator, "Decision Tree Learning Rate", X_train, y_train, axes=[axes], ylim=(0.6, 1.01),
3                    cv=cv, n_jobs=4)


# ## SVM Model

# In[12]:


svm = SVC(kernel='poly',
          C=1,
          degree=3,
          random_state=0)
svm.fit(X_train, y_train)

y_train_pred = svm.predict(X_train)
y_test_pred = svm.predict(X_test)

print("Using Poly Kernel:")
print('SVM Train Accuracy' , accuracy_score(y_train, y_train_pred))
print('SVM Test Accuracy' , accuracy_score(y_test, y_test_pred))

cv = KFold(n_splits=10, random_state=0, shuffle=True)
fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(svm, "Initial Poly Learning Rate", X_train, y_train, axes=[axes], ylim=(0.25, 0.8),
                    cv=cv, n_jobs=4)


# ## GridSearch poly kernel SVM parameters

# In[13]:


svm_params1 = {
    "kernel":['poly'],
    "degree":range(1,5),
    "C":[0.01,0.1,1,10]
}

svm = SVC()
grid = GridSearchCV(svm,
                    param_grid = svm_params1,
                    cv=10,
                    verbose=1,
                    n_jobs=-1
)
grid.fit(X_train,y_train)
print(grid.best_params_)


# In[14]:


svm_params1 = {
    "kernel":['rbf'],
    "gamma":['scale','auto']
}

svm = SVC()
grid = GridSearchCV(svm,
                    param_grid = svm_params1,
                    cv=10,
                    verbose=1,
                    n_jobs=-1
)
grid.fit(X_train,y_train)
print(grid.best_params_)


# In[15]:


start_time = time.time();
svm = SVC(kernel='rbf',
          gamma='scale',
          random_state=0)
svm.fit(X_train, y_train)

print("Execution Time:",time.time()-start_time)

start_time = time.time();
y_train_pred = svm.predict(X_train)
y_test_pred = svm.predict(X_test)
print("Prediction Time:",time.time()-start_time, "for",len(X_train) + len(X_test),"samples")

print("\nUsing RBF Kernel:")
print('SVM Train Accuracy' , accuracy_score(y_train, y_train_pred))
print("SVM Train r2 score:",r2(y_train, y_train_pred))
print('SVM Test Accuracy' , accuracy_score(y_test, y_test_pred))
print("SVM Test r2 score:",r2(y_test, y_test_pred))
print("F1 SCORE:",round(f1_score(y_test, y_test_pred, average='micro'),3))


# In[16]:


cv = KFold(n_splits=10, random_state=0, shuffle=True)
fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(svm, "RBF Learning Rate", X_train, y_train, axes=[axes], ylim=(0.5, 1.01),
                    cv=cv, n_jobs=4)


# In[17]:


start_time = time.time();
svm = SVC(kernel='poly',
          C=10,
          degree=2,
          random_state=0)
svm.fit(X_train, y_train)

print("Execution Time:",time.time()-start_time)

start_time = time.time();
y_train_pred = svm.predict(X_train)
y_test_pred = svm.predict(X_test)
print("Prediction Time:",time.time()-start_time, "for",len(X_train) + len(X_test),"samples")

print("Using Poly Kernel:")
print('SVM Train Accuracy' , accuracy_score(y_train, y_train_pred))
print("SVM Train r2 score:",r2(y_train, y_train_pred))
print('SVM Test Accuracy' , accuracy_score(y_test, y_test_pred))
print("SVM Test r2 score:",r2(y_test, y_test_pred))
print("F1 SCORE:",round(f1_score(y_test, y_test_pred, average='micro'),3))


# In[18]:


cv = KFold(n_splits=10, random_state=0, shuffle=True)
fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(svm, "Poly Learning Rate", X_train, y_train, axes=[axes], ylim=(0.7, 1.01),
                    cv=cv, n_jobs=4)


# ## KNN Algorithm

# In[19]:


knn = KNeighborsClassifier(n_neighbors=10,
                           algorithm='auto',
                           leaf_size=10)
knn.fit(X_train, y_train)

y_train_pred = knn.predict(X_train)
y_test_pred = knn.predict(X_test)

print('KNN Train Accuracy' , accuracy_score(y_train, y_train_pred))
print('KNN Test Accuracy' , accuracy_score(y_test, y_test_pred))

cv = KFold(n_splits=10, random_state=0, shuffle=True)
fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(knn, "Initial KNN Learning Rate", X_train, y_train, axes=[axes], ylim=(0.45, 0.85),
                    cv=cv, n_jobs=4)


# In[20]:


knn_params = {
    "n_neighbors":range(1,10),
    "leaf_size":range(25,35),
    "algorithm":['auto', 'ball_tree', 'kd_tree', 'brute']
}

knn = KNeighborsClassifier()
grid = GridSearchCV(knn,
                    param_grid = knn_params,
                    cv=10,
                    verbose=1,
                    n_jobs=-1
)
grid.fit(X_train,y_train)
print(grid.best_params_)


# In[21]:


start_time = time.time();
knn = KNeighborsClassifier(n_neighbors=4,
                           algorithm='auto',
                           leaf_size=33)
knn.fit(X_train, y_train)

print("Execution Time:",time.time()-start_time)


start_time = time.time();
y_train_pred = knn.predict(X_train)
y_test_pred = knn.predict(X_test)

print("Prediction Time:",time.time()-start_time, "for",len(X_train) + len(X_test),"samples")

print("K = 4")
print('Decision Tree Train Accuracy' , accuracy_score(y_train, y_train_pred))
print("Decision Tree Train r2 score:",r2(y_train, y_train_pred))
print('KNN Test Accuracy' , accuracy_score(y_test, y_test_pred))
print("KNN Test r2 score:",r2(y_test, y_test_pred))
print("F1 SCORE:",round(f1_score(y_test, y_test_pred, average='micro'),3))


# In[22]:


cv = KFold(n_splits=10, random_state=0, shuffle=True)
fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(knn, "KNN Learning Rate", X_train, y_train, axes=[axes], ylim=(0.45, 0.8),
                    cv=cv, n_jobs=4)


# In[23]:


booster = AdaBoostClassifier(
    tree.DecisionTreeClassifier(max_depth=2, 
                                  criterion='entropy',
                                  min_samples_leaf=1,
                                  min_samples_split=2,
                                  random_state=0),
    n_estimators=50,
    learning_rate=0.1,
    random_state=0) 
booster.fit(X_train, y_train)

y_test_pred = booster.predict(X_test)
y_train_pred = booster.predict(X_train)
print('Boosted Tree Train Accuracy' , accuracy_score(y_train, y_train_pred))
print('Boosted Tree Test Accuracy' , accuracy_score(y_test, y_test_pred))

cv = KFold(n_splits=10, random_state=0, shuffle=True)
fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(booster, "Initial ADA Booster Learning Rate", X_train, y_train, axes=[axes], ylim=(0.55, 0.95),
                    cv=cv, n_jobs=4)


# In[24]:


start_time = time.time();
booster = AdaBoostClassifier(
    tree.DecisionTreeClassifier(max_depth=6,#Tested values between 1 and 20
                                  criterion='gini',
                                  random_state=0,
                                  min_samples_leaf=1,
                                  min_samples_split=3),
    n_estimators=500,
    learning_rate=0.5,#0.5: 0.908  #tested values between 0.01 and 1
    random_state=0)
booster.fit(X_train, y_train)

print("Execution Time:",time.time()-start_time)
start_time = time.time();

y_test_pred = booster.predict(X_test)
y_train_pred = booster.predict(X_train)
print("Prediction Time:",time.time()-start_time, "for",len(X_train) + len(X_test),"samples")

print('Boosted Tree Train Accuracy' , accuracy_score(y_train, y_train_pred))
print("Boosted Tree Train r2 score:",r2(y_train, y_train_pred))
print('Boosted Tree Test Accuracy' , accuracy_score(y_test, y_test_pred))
print("Boosted Tree Test r2 score:",r2(y_test, y_test_pred))
print("F1 SCORE:",round(f1_score(y_test, y_test_pred, average='micro'),3))


# In[25]:


if False:#Turn to true to run cell.  Takes > 15 minutes.
    start_time = time.time();
    train_accuracies = [];
    test_accuracies = [];
    for i in range(1,1001,50):
        booster = AdaBoostClassifier(
            tree.DecisionTreeClassifier(max_depth=6,#Tested values between 1 and 20
                                      criterion='gini',
                                      random_state=0,
                                      min_samples_leaf=1,
                                      min_samples_split=3),
            n_estimators=i,
            learning_rate=0.5,
            random_state=0) 
        booster.fit(X_train, y_train)
        y_test_pred = booster.predict(X_test)
        y_train_pred = booster.predict(X_train)
        train_accuracies.append(accuracy_score(y_train, y_train_pred))
        test_accuracies.append(accuracy_score(y_test, y_test_pred))
        print(i,end=" ")
    print("Training Scores:",train_accuracies)
    print("Testing Scores:",test_accuracies)


# In[26]:


plt.plot(range(1,1001,50),train_accuracies,marker='o',label="Training Set")
plt.plot(range(1,1001,50),test_accuracies,marker='o',label="Testing Set")
plt.xlabel('Estimators')
plt.ylabel('Classification Accuracy')
plt.title("Adaptive Boosting Accuracy by Estimator Count")
plt.legend()
plt.show()


# In[27]:


cv = KFold(n_splits=10, random_state=0, shuffle=True)
fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(booster, "ADA Booster Learning Rate", X_train, y_train, axes=[axes], ylim=(0.8, 1.01),
                    cv=cv, n_jobs=4)


# ## Neural Network

# In[28]:


nn = MLPClassifier(activation='relu',
                   hidden_layer_sizes=(32,),
                   solver='lbfgs',
                   verbose=True,
                   max_iter=50,
                   random_state=1,
                   early_stopping=True)

nn.fit(X_train, y_train)
y_test_pred = nn.predict(X_test)
y_train_pred = nn.predict(X_train)

print('Neural Network Train Accuracy' , accuracy_score(y_train, y_train_pred))
print('Neural Network Test Accuracy' , accuracy_score(y_test, y_test_pred))

cv = KFold(n_splits=10, random_state=0, shuffle=True)
fig, axes = plt.subplots(1,1, figsize=(8, 5))
plot_learning_curve(nn, "Initial Neural Network Learning Rate", X_train, y_train, axes=[axes], ylim=(0.65, 1.01),
                    cv=cv, n_jobs=4)


# In[29]:


nn_params = {
    "hidden_layer_sizes":[(64,),(),(64,32),(32,)],
    "activation":['identity', 'logistic', 'tanh', 'relu'],
    "solver":['lbfgs', 'sgd', 'adam'],
    "verbose":[True]
}

nn = MLPClassifier()
grid = GridSearchCV(nn,
                    param_grid = nn_params,
                    cv=10,
                    verbose=1,
                    n_jobs=-1
)
grid.fit(X_train,y_train)
print(grid.best_params_)


# In[30]:


start_time = time.time();
nn = MLPClassifier(activation='relu',
                   hidden_layer_sizes=(64,),
                   solver='lbfgs',
                   verbose=True,
                   max_iter=200,
                   random_state=1,
                   early_stopping=True)

nn.fit(X_train, y_train)
print("Execution Time:",time.time()-start_time)
start_time = time.time();

y_test_pred = nn.predict(X_test)
y_train_pred = nn.predict(X_train)
print("Prediction Time:",time.time()-start_time, "for",len(X_train) + len(X_test),"samples")

print('Neural Network Train Accuracy' , accuracy_score(y_train, y_train_pred))
print('Neural Network Train r2 score:',r2(y_train, y_train_pred))
print('Neural Network Test Accuracy' , accuracy_score(y_test, y_test_pred))
print("Neural Network Test r2 score:",r2(y_test, y_test_pred))
print("F1 SCORE:",round(f1_score(y_test, y_test_pred, average='micro'),3))


# In[31]:


nn_scores_train = [];
nn_scores = [];
for i in range(20):
    nn = MLPClassifier(activation='relu',
                   hidden_layer_sizes=(64,),
                   solver='lbfgs',
                   verbose=False,
                   max_iter=i*10+1,
                   random_state=1)
    
    nn.fit(X_train,y_train);
    y_test_pred = nn.predict(X_test)
    nn_scores_train.append(accuracy_score(y_train, y_train_pred))
    nn_scores.append(accuracy_score(y_test, y_test_pred))
    print("I",i)
print("Training Scores:",nn_scores_train)
print("Testing Scores:",nn_scores)


# In[32]:


plt.plot(range(1,201,10),nn_scores_train,marker='o',label="Training Set")
plt.plot(range(1,201,10),nn_scores,marker='o',label="Testing Set")
plt.xlabel('Network Iterations')
plt.ylabel('Classification Accuracy')
plt.title("Neural Network Accuracy by Iteration Number")
plt.legend()
plt.show()

